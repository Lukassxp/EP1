#ifndef FILTRO_HPP
#define FILTRO_HPP

#include "cor.hpp"
#include "imagem.hpp"

#include <iostream>
#include <list>

using namespace std;

class Filtro{
	protected:
		list<Cor> pixels;
		list<Cor>::iterator controlador;
		class Cor ajudante;
	public:
		Filtro();
		void setPixels(list<Cor> pixels);
		void setAjudante(class Cor ajudante);
		list<Cor> getPixels();
		class Cor getAjudante();
		virtual list<Cor> aplicaFiltro(list<Cor> cores, int maxEscala);
};
#endif
