#ifndef FILTROPRETOBRANCO_HPP
#define FILTROPRETOBRANCO_HPP

#include "filtro.hpp"
#include "imagem.hpp"
#include "cor.hpp"

#include <iostream>
#include <list>

using namespace std;

class FiltroPretoBranco : public Filtro{
	public:
		FiltroPretoBranco();
		list<Cor> aplicaFiltro(list<Cor> cores, int maxEscala);
};
#endif

