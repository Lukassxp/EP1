#include "filtroMedia.hpp"
#include <stdio_ext.h>

FiltroMedia::FiltroMedia(){

}

void FiltroMedia::setAltura(int altura){
	this->altura = altura;
}

void FiltroMedia::setLargura(int largura){
	this->largura = largura;
}

list<Cor> FiltroMedia::aplicaFiltro(list<Cor> cores, int maxEscala){
	int i, j, x, y, limit, div, opcao;
	unsigned char aux = maxEscala;

	cout << "\nMatrizes de media disponiveis: \n\n1 - 3x3\n2 - 5x5\n3 - 7x7\n\n";
	
	do{
		cout << "Qual matriz deseja usar? ";
		__fpurge(stdin);
		scanf("%i",&opcao);
		__fpurge(stdin);
		if(opcao < 1 || opcao > 3){
			cout << "\nOpcao invalida, deve ser um numero de 1 a 3!" << endl;
		}
	}while(opcao < 1 || opcao > 3);

	switch(opcao){
		case 1: limit = 1;
			div = 9;
			break;
		case 2: limit = 2;
			div = 25;
			break;
		case 3: limit = 3;
			div = 49;
			break;
	}

	for(i = limit; i < altura - limit; ++i){
    		for(j = limit; j < largura - limit; ++j) {
           		ajudante.setR(0);
			ajudante.setG(0);			
			ajudante.setB(0);

           		for(x = -limit; x <= limit; x++) {
                    		for(y = -limit; y <= limit;y++){
                          		ajudante.setR(ajudante.getR() + matriz[x+i][y+j].getR() / div);
                          		ajudante.setG(ajudante.getG() + matriz[x+i][y+j].getG() / div);
                          		ajudante.setB(ajudante.getB() + matriz[x+i][y+j].getB() / div);
                    		}
           		}
           		matriz[i][j].setR(ajudante.getR());
			matriz[i][j].setG(ajudante.getG());
			matriz[i][j].setB(ajudante.getB());
    		}		
	}

	i = 0;
	j = 0;

	ajudante.setR(0);
	ajudante.setG(0);			
	ajudante.setB(0);

	for(controlador = cores.begin(); controlador != cores.end(); controlador++){
		ajudante = matriz[i][j];

		j++;

		if(j == largura){
			i++;
			j = 0;
		}	

		pixels.push_back(ajudante);

		if(i == altura){
			cout << "\nFiltro aplicado com sucesso!\n" << endl;
			return pixels;
		}
	}
	return pixels;
}

void FiltroMedia::constroiMatriz(list<Cor> cores){
	int i = 0, j = 0;
	
  	matriz = (Cor**)malloc(altura * sizeof(Cor*));
 
  	for (i = 0; i < altura; i++){
      		matriz[i] = (Cor*)malloc(largura * sizeof(Cor));

       		for (j = 0; j < largura; j++){
           		matriz[i][j].setR(0);
			matriz[i][j].setG(0);
			matriz[i][j].setB(0);
       		}
  	}
	
	i = 0;
	j = 0;

	for(controlador = cores.begin(); controlador != cores.end(); controlador++){
		matriz[i][j] = *controlador;

		j++;

		if(j == largura){
			i++;
			j = 0;
		}
		if(i == altura){
			return;
		}	
	}
}
