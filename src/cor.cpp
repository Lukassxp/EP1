#include "cor.hpp"

Cor::Cor(){

}

Cor::Cor(unsigned char r, unsigned char g, unsigned char b){
	this->r = r;
	this->g = g;
	this->b = b;
}

unsigned char Cor::getR(){
	return r;
}

unsigned char Cor::getG(){
	return g;
}

unsigned char Cor::getB(){
	return b;
}

void Cor::setR(unsigned char r){
	this->r = r;
}

void Cor::setG(unsigned char g){
	this->g = g;
}

void Cor::setB(unsigned char b){
	this->b = b;
}
