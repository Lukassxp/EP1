#include "imagem.hpp"
#include "cor.hpp"

Imagem::Imagem(){

}

Imagem::Imagem(string formato, int altura, int largura, int maxEscala, list<Cor> cores){
	this->formato = formato;
	this->altura = altura;
	this->largura = largura;
	this->maxEscala = maxEscala;
	this->cores = cores;
}

void Imagem::setDiretorio(string diretorio){
	this->diretorio = diretorio;
}

void Imagem::setFormato(string formato){
	this->formato = formato;
}

void Imagem::setAltura(int altura){
	this->altura = altura;
}

void Imagem::setLargura(int largura){
	this->largura = largura;
}

void Imagem::setMaxEscala(int maxEscala){
	this->maxEscala = maxEscala;
}

void Imagem::setFimCabecalho(int fimCabecalho){
	this->fimCabecalho = fimCabecalho;
}

void Imagem::setCores(list<Cor> cores){
	this->cores = cores;
}

string Imagem::getDiretorio(){
	return diretorio;
}

string Imagem::getFormato(){
	return formato;
}

int Imagem::getAltura(){
	return altura;
}

int Imagem::getLargura(){
	return largura;
}

int Imagem::getMaxEscala(){
	return maxEscala;
}

int Imagem::getFimCabecalho(){
	return fimCabecalho;
}

list<Cor> Imagem::getCores(){
	return cores;
}

void Imagem::lerImagem(ifstream *arquivo){
	unsigned char aux;
	string nomeImagem;
	Cor *cor = new Cor();

	do{
		cout << "\nInsira o nome do arquivo de imagem a que deseja aplicar um filtro: ";
		cin >> nomeImagem;

		diretorio = "doc/" + nomeImagem + ".ppm";
		arquivo->open(diretorio.c_str());

		if(arquivo->fail()){
			cout << "\n\nErro na abertura de arquivo, verifique o nome!" << endl;
		}
	}while(arquivo->fail());

	ignorarComentario(arquivo);
	(*arquivo) >> formato;
	ignorarComentario(arquivo);
	(*arquivo) >> largura;
	(*arquivo) >> altura;
	ignorarComentario(arquivo);
	(*arquivo) >> maxEscala;
	ignorarComentario(arquivo);

	fimCabecalho = arquivo->tellg();
	fimCabecalho = fimCabecalho - 1;

	while(!arquivo->eof()){
		cor->setR(aux = arquivo->get());
		cor->setG(aux = arquivo->get());
		cor->setB(aux = arquivo->get());

		cores.push_back((*cor));
	}

	cout << "\nInformacoes da imagem:\n" <<"\nFormato: " << getFormato() << "\nDimensoes: " << getAltura() << " X " << getLargura();
	cout <<"\nEscala Maxima: " << getMaxEscala() << "\nTamanho do Cabecalho: " << getFimCabecalho() << "\n\nImagem carregada com sucesso!\n" << endl;
	
}

void Imagem::gravarImagem(ofstream *novoArquivo, ifstream *arquivo){
	int flag = 0;
	string nomeImagem;
	Cor cor;
	list<Cor>::iterator it;

	do{
		cout << "\nInsira o nome que deseja dar a imagem com filtro aplicado: ";
		cin >> nomeImagem;

		nomeImagem = "doc/" + nomeImagem + ".ppm";
		arquivo->open(nomeImagem.c_str());

		if(arquivo->fail() == 0){
			cout << "\n\nEste arquivo ja existe, verifique o nome!" << endl;
			arquivo->close();
			flag = 1;
		}
		else{
			novoArquivo->open(nomeImagem.c_str());
			flag = 0;
			if(novoArquivo->fail()){
				cout << "\n\nErro na criacao do arquivo!" << endl;
				flag =1;
			}
		}
	}while(flag != 0);

	arquivo->open(diretorio.c_str());

	while(arquivo->tellg() <= fimCabecalho){
		novoArquivo->put((unsigned char)arquivo->get());
	}

	for(it = cores.begin(); it != cores.end(); it++){
		cor = *it;

		novoArquivo->put(cor.getR());
		novoArquivo->put(cor.getG());
		novoArquivo->put(cor.getB());
	}
	cout << "\nImagem filtrada salva com sucesso!\n" << endl;

}

void Imagem::ignorarComentario(ifstream * arquivo){
	string linha;
	(*arquivo) >> linha;
	
	if(linha[0] != '#'){
		arquivo->seekg(-linha.size(),ios_base::cur);
		return;
	}

	while(linha[0] == '#'){
		getline(*arquivo,linha);
		(*arquivo) >> linha;
	}
	arquivo->seekg(-linha.size(),ios_base::cur);
}
