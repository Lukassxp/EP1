#include "filtroPolarizado.hpp"

FiltroPolarizado::FiltroPolarizado(){

}

list<Cor> FiltroPolarizado::aplicaFiltro(list<Cor> cores, int maxEscala){
	unsigned char aux = maxEscala;

	for(controlador = cores.begin(); controlador != cores.end(); controlador++){
		ajudante = *controlador;

		if(ajudante.getR() < aux/2){
        		ajudante.setR(0);
        	}else{
        		ajudante.setR(aux);
        	}

        	if(ajudante.getG() < aux/2){
        		ajudante.setG(0);
        	}else{
        		ajudante.setG(aux);
        	}

        	if(ajudante.getB() < aux/2){
        		ajudante.setB(0);
        	}else{
        		ajudante.setB(aux);
        	}

		pixels.push_back(ajudante);
	}
	cout << "\nFiltro aplicado com sucesso!\n" << endl;
	return pixels;
}
