#include "filtro.hpp"

Filtro::Filtro(){

}

void Filtro::setPixels(list<Cor> pixels){
	this->pixels = pixels;
}

void Filtro::setAjudante(class Cor ajudante){
	this->ajudante = ajudante;
}

list<Cor> Filtro::getPixels(){
	return pixels;
}

class Cor Filtro::getAjudante(){
	return ajudante;
}

list<Cor> Filtro::aplicaFiltro(list<Cor> cores, int maxEscala){
	return cores;
}
